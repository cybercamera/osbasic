# osBasic - Old School BASIC

A long time ago in a land far, far away....


![osBasic Screen](https://gitlab.com/cybercamera/osbasic/-/raw/main/video1.gif "osBasic Screen")

## Welcome to osBasic

osBasic is a very simple editor and run-time environment for the powerful 
GAMBAS object-oriented Basic language. osBasic exists to give newcomers 
to programming a taste of what coding was like in simpler times. It also 
functions as a simple editor for writing scripts in gbscripter.

For more information on the language used to both build osBasic and to 
write code in it, visit: http://gambas.sourceforge.net/en/main.html

As with many good things, osBasic is open source software, and free for
you to use and hack on. To gain inspiration, press the F2 key to see 
code-snippets that you can learn from and incorporate in your own code.
To run your own code, enter at a command line: gbs3 <your_code_filename.gbs> 


## Why does this tool even exist?

osBasic was intended to give a newcomer to coding the simplest possible
learning-curve, yet still make available a fully-functional (dare we say
powerful?) programming language. In many ways, osBasic was designed to give new
generations of coders a feel for what coding was like back in the days when
8-bit microcomputers ruled the Earth; in those days spirits were brave, 
the stakes were high, men were real men, women were real women and small 
furry creatures from Alpha Centauri were...ahem. 

So, anyway, Old School BASIC, if you prefer.

Also, because most people hate to read documentation, were going to make this
help file short. Really short. So, here it is.

osBasic is a programming development environment. In osBasic, you can write
code using a modern variant of the BASIC (Beginners All-purpose Symbolic
Instruction Code), save it, load it and most importantly, run it. osBasic comes
with a rich developers text editor. In order to keep the learning-curve for
newcomers *really* shallow however, most of that richness has been hidden. In
fact, we really only want you to care about a handful of functions: writing
code, saving code, loading code and running code. Oh, and calling this help
screen.

## The Short Version

Pressing the key(s):
* F1 (or Ctrl+h)  - shows this help file
* F2              - shows the Code Snippets
* F3 (or Ctrl+o)  - shows the code load dialog box to allow you to open your code files
* F4 (or Ctrl+s)  - shows the code save dialog box to allow you to save your code files
* F5 (or Ctrl+r)  - opens the runtime environemt and runs your code and shows you the results
* Ctrl+c          - copies any code text youve selected (but you knew that, right?)
* Ctrl+x          - cuts any code text youve selected
* Ctrl+v          - pastes any code text youve selected
* Ctrl+a          - selects all of the code text in the current editor
* Ctrl+f          - shows the Find tool which helps you find text in the current editor
* Ctrl+t          - spawns a new editor tab
* Esc             - takes you to the command-line at the bottom of the screen
* Ctrl+q          - quits the program

## Loading/Saving/Running Code

Enter code directly in the editor. You can also spawn a new editor so that you
can edit two programs at once. Or many new editors. Your choice. You can close
them by clicking the little red circle in tab along the top of the editor. 

If you prefer command lines to pressing keyboard-accellerator combinations,
entering: 'load myfile.gbs' (without the quotes, please) at the command prompt
at the bottom will load that code file from your code directory. Ditto: 'save
myfiile.gbs' will save the current editor file to that filename. And entering
run will run the program.

When run, the program will show up in the shell. Now, this is a full shell to
the underlying operating system, so mind what you do in it. Running your
program again - or any other program for that matter - will run that code too in
this same shell. Now, if you want, you can keep to text-based programming,
whereby all of your code is presented to you running within that shell.
However, osBasic is fully able to run graphical programs too, should you so
wish to delve. Examples, as with every other bit of coding help available in
osBasic, are provided in the Code Snippets.

osBasic is a very simple program with a very simple purpose - to remove all barriers
to programming for newcomers. When youi've reached a point of sufficient
expertise and comfort, you can move onto the actual programming environemt (and
the full functionality of the accompanying programming language) which
underpins osBasic: Gambas ( http://gambas.sourceforge.net/en/main.html ) However,
you can also delve into bits of the Gambas programming language that you need
to write the programs you need to write in osBasic. Heres the documentation
for that: http://gambas.sourceforge.net/en/main.html

## Code Snippets

Code Snippets is how you get to actually learning anything from osBasic.
They're an array of code blocks that you can copy/inject into your own
programs, figure out how they work, extend, break and eventually fix. They
start from simple one-liner programs, to programs that allow you to ask a user
for input, to programs that open up graphical windows, etc. 

A Code Snippet is just a standard osBasic code file, with the following text 
at the start of the file: 'DESCRIPTION:, followed by an actual (simple) 
description of what the code snippet actually does. As such, you can create 
your own code snippets and save them, and they will be shown to you when you 
press the F2 key. That way, you can build up your own library of code. 
Perhaps even consider contributing them back, so that others can learn from 
your efforts.

# Installation

Hopefully, the pre-packaged versions should have all you need; your package 
manager will do the rest. However, if you're having issues with the dependencies - or 
if you prefer to use the tarball approach to installations - you can add the following 
packages manually:

```
gambas3-runtime, gambas3-gb-args, gambas3-gb-eval-highlight, gambas3-gb-form, gambas3-gb-form-editor, gambas3-gb-form-terminal, gambas3-gb-image, gambas3-gb-term, gambas3-gb-util, gambas3-gb-scripter, gambas3-gb-qt4 

```

Generally, you can add these packages via your standard package manager, for example, 
on Ubuntu derived systems that would be something like:

```
sudo apt install gambas3-runtime gambas3-gb-args gambas3-gb-eval-highlight gambas3-gb-form gambas3-gb-form-editor gambas3-gb-form-terminal gambas3-gb-image gambas3-gb-term gambas3-gb-util gambas3-gb-scripter gambas3-gb-qt4 

```

Well, thats about it. As the Intro screen mentions, osBasic is free and open
source software (GPL3). It too is available for you to learn how to code/extend
it: https://gitlab.com/cybercamera/osbasic

Happy hacking!

ps: You can also get pre-built packages (Ubuntu, Debian, Fedora, Suse, Mageia, generic) from here: https://gitlab.com/cybercamera/osbasic-binaries

